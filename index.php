<?php 
    include "class.php";
    include "db.php";
    include "query.php"
?>
<html>
 <head>
 <title> object oriented PHP </title>
 </head>

 <body>
 <p>
 <?php
 // הרצאה 1
    $text = 'hello world';
    echo "$text and the universe";
    echo '<br>';
    $msg = new Message ("");
    //echo $msg->text;
    $msg->show();
    echo Message:: $count; 
    $msg1 = new Message("A new text");
    $msg1->show();
    echo Message:: $count; 
    $msg1 = new Message();
    $msg1->show(); 
    echo '<br>';
    echo Message:: $count;
    // הרצאה 2 
    $msg3 = new redMessage ('A red message');
    $msg3->show();
    echo '<br>';
    $msg4 = new coloredMessage('A color message');
    $msg4->color = 'green';
    $msg4->show();
    echo '<br>';
    $msg5 = new coloredMessage('A color message');
    $msg5->color = 'yellow';
    $msg5->show();
    echo '<br>';
    showObject($msg4);
    echo '<br>';
    showObject($msg1);  
    // הרצאה 4
    // database connection
    $db = new DB('localhost', 'intro', 'root', ''); 
    $dbc =$db -> connect();
    //query for example
    $query = new Query($dbc);
    $q = "SELECT * FROM users";
    $result = $query -> query($q);
    echo '<br>';
    //echo $result ->num_rows; מספר השורות בשאילתא
    if($result ->num_rows >0 ){ //הדפסת השאילתא
        echo '<table>';
        echo '<tr><th>Name</th><th>email</th></tr>';
        while($row = $result ->fetch_assoc()){
            echo '<tr>';   
            
            echo '<td>'.$row ['name'].'</td><td>'.$row ['email'].'</td>';

            echo '</tr>';

        }
        echo '</table>';
    }
    else{
        echo "Sorry no results";   
    }

    ?>
 </p>
 </body>
</html>